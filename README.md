PhoneGapTestRun Androidライブラリ
================================

Eclipse+AndroidSDKでPhoneGap用のプロジェクトにQUnitなどのテストフレームワークを利用する場合、テスト用のindex.htmlをPCのコマンド上からロードするライブラリ。

利用方法
---------

1. テストを行いたいプロジェクトと同じワークスペースに、PhoneGapTestRunのプロジェクトをインポートする

2. インポートをした後にEclipse上のメニューで `Project/Clean` を選択して、インポートしたプロジェクトのCleanupを行う(このとき `gen` が無いというようなエラーが出たら、エクスプローラーなどでプロジェクトのルートに `gen` フォルダを作成し何度かCleanを実行する)。

3. インポートしたプロジェクトを右クリックし、 `Android Tools/Fix Project Properties` を選択する(これでプロジェクトのエラーが消えるはず)。

4. テストを行いたいプロジェクトの `assets/www/tests/` フォルダに、テスト用の `index.html` などを一式配置する。

5. テストを行いたいプロジェクトのプロパティからAndroidタブを開き、Library一覧に `PhoneGapTestRun` プロジェクトを追加する。

6. テストを行いたいプロジェクトの `AndroidManifest.xml` を開き、application要素の中に以下のコードを追加する。

    ```
    <activity android:name="com.fc2.blog23.zawakei.test.phonegaptestrun.Test" android:label="@string/app_name">
    	<intent-filter>
    	</intent-filter>
    </activity>
    ```

7. テストを行いたいプロジェクトをEclipse上からRunするなどどして、通常通りに動作するか確認する。

8. PC上のコマンドプロンプトから `adb shell am start -n <PackageName>/com.fc2.blog23.zawakei.test.phonegaptestrun.Test` を実行する( `<PackageName>` をテストを行いたいプロジェクトのPackageNameに置き換える)。

9. Android上でテストの実行画面が表示される。

備考
----

詳細は「[PhoneGap上でテストを実行する方法を考えてみる](http://zawakei.blog23.fc2.com/blog-entry-346.html)」を参照。